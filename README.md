# Lerneinheit FAIRe Qualitäts-KPIs

## Einführung
Siehe Skript und Aufgabenstellung in [moodle](https://moodle.tu-darmstadt.de/course/view.php?id=36368&section=3#tabs-tree-start).

## Materialien
In diesem GitLab Repo finden Sie:
- Package functions (`functions/`): Beinhaltet die Module `classes` und `calculation_rules`
- Modul classes (`functions/classes.py`): Werkzeuge zum Aufbau und zur Bearbeitung der LEGO-Konstruktionen
- Modul calculation_rules (`functions/calculation_rules.py`): Funktionen zum Berechnen der FAIRen
Qualitäts-KPIs
- Python-Hilfsdatei (`functions/__init__.py`): Notwendige Datei für die Erzeugung eines Python-Pakets
- Datenblätter (`datasheets/`): Datenblätter ausgewählter LEGO-Komponenten im JSON-Format
- Teilebibliothek (`LeoCAD/library.bin`): Teilebibliothek für das Programm LeoCAD
- Abbildungsordner (`figures`): Ordner zum Ablegen der LeoCAD-Screenshots
- Beispiel-Notebook (`minimalbeispiel.ipynb`): Einführendes Beispiel zur Verwendung der Werkzeuge
- Ausarbeitungs-Notebook (`ausarbeitung.ipynb`): Vorlage zur Bearbeitung der Aufgaben und Abgabe

## Ausarbeitung
Die Ausarbeitung erfolgt im Notebook `ausarbeitung.ipynb`. In diesem ist bereits eine Gliederung vorgegeben.

## Abgabe
Die Abgabe erfolgt über [moodle](https://moodle.tu-darmstadt.de/mod/assign/view.php?id=1249192). Committen und pushen Sie zunächst Ihre Änderungen auf GitLab und laden Sie von dort Ihr gesamtes Repo als .zip-Datei herunter (ein direkter Download vom JupyterHub ist leider nicht möglich). Benennen Sie die .zip-Datei nach dem folgenden Schema:
<p style="text-align: center;"> &lt;Nachname&gt;_&lt;Vorname&gt;_&lt;MATR-NR&gt;_&lt;GRUPPEN-NR&gt;_le_1.zip</p>

Abgaben, die diese Namenskonvention nicht erfüllen, können in der Bewertung nicht berücksichtigt werden.
Laden Sie diese .zip-Datei in moodle hoch. Insbesondere sollten vorhanden sein:
- Jupyter Notebook mit Erklärungen und Darstellungen von LeoCAD
- Python-Funktionen für die Berechnung der KPIs
- Die exportierte(n) JSON-Datei(en) des Fahrzeuges (inkl. der hinzugefügten KPIs)